# Summary Biomedical Imaging HS16
Summary of the Imaging lecture series of fall 2016 held by Kozerke, Prüssman, Rudin with some additional info from the lecture book *Introduction to Medical Imaging: Physics, Engineering and Clinical Applications (A. Webb)*. 
Designed to be printed onto one double-sided A4 page and still be readable.
